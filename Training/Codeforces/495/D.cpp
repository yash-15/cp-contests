#include <bits/stdc++.h>
using namespace std;

int a[1000006];
vector<int> f;

#define cnt(r,c,val) max(0,min(r,min(c,min(val+1,r+c-1-val))))


int cntTotal(int r,int c, int h, int k, int val){
	int ans= cnt(h+1,k+1,val)+cnt(h+1,c-k,val)
			+cnt(r-h,k+1,val)+cnt(r-h,c-k,val)
			-cnt(h+1,1,val)-cnt(k+1,1,val)
			-cnt(r-h,1,val)-cnt(c-k,1,val)
			+cnt(1,1,val);
	//cout<<"     "<<val<<" :-> "<<ans<<"\n";
	return ans;
}

bool ok(int r, int c, int h, int k, int mx){
	//cout<<"  Trying centre = "<<h<<","<<k<<"\n";
	if (cntTotal(r,c,h,k,mx+1)>0)
		return 0;

	for(int val = mx;val>=0;val--){
		if(cntTotal(r,c,h,k,val)!=f[val])
			return 0; 
	}
	//cout<<"   This centre works! OK!";
	return 1;
}

pair<int,int> solve(int r, int c, int d){
	int i,j;
	//cout<<"Solving for size: "<<r<<" x "<<c<<"\n";
	for(i = max(0,d-c+1);i<r;i++){
		j = d-i;
		if(ok(r,c,i,j,d))
			return {i+1,j+1};
	}
	return {-1,-1};
}

int main(){
	int t,i,mx = -1;
	cin >> t;
	for(i=0;i<t;i++){
		scanf("%d",a+i);
		if(a[i]>mx)
			mx=a[i];
	}
	f.resize(mx+1);
	for(i=0;i<t;i++)
		f[a[i]]++;
	//for(i=0;i<=mx;i++)
	//	cout<<i<<" :: "<<f[i]<<"\n";

	for(i=1;i*i<=t;i++){
		if(t%i == 0){
			auto res = solve(i,t/i,mx);
			if(res.first>0){
				cout << i<<" "<<(t/i)<<"\n"
					 << res.first<<" "<<res.second<<"\n";
				return 0;
			}
		}
	}
	cout<<"-1"<<"\n";

}