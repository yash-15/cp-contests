#include <bits/stdc++.h>
using namespace std;

int a[100005];
map<int,int> l,r;
int main(){		
	int n,i,j;
	cin >> n;
	for(i=0;i<n;i++)
		scanf("%d",a+i);
	for(i=0;i<n;i++){
		if(!l.count(a[i]))
			l[a[i]] = i+1;
		r[a[i]]=i+1;
	}
	vector<int> lt,rt;
	for(auto &x:l)
		lt.push_back(x.second);
	for(auto &x:r)
		rt.push_back(x.second);
	sort(lt.begin(), lt.end());
	sort(rt.begin(), rt.end());
	int n2 = lt.size();
	long long ans = 0;
	for(i=0,j=0;i<n2;i++){
		while(j<n2 && lt[i]>=rt[j])
			j++;
		ans+=(n2-j);
	}
	cout << ans << "\n";
}