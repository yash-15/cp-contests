#include <bits/stdc++.h>
using namespace std;
int main(){
	int n,m,u,v;
	cin >> n >> m;
	int l=1,r=n;
	while(m--){
		scanf("%d%d",&u,&v);
		if(u>v)
			swap(u,v);
		l=max(l,u);
		r=min(r,v);
	}
	cout<<max(r-l,0)<<"\n";
}
