#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define inf 1e17
#define plii pair<ll,pair<int,int> >

class Edge{
public:
	int v,w,i;
	Edge(int _v, int _w, int _i)
	: v(_v),w(_w),i(_i) {}
};

vector<Edge> e[300005];

ll d[300005];
bool vis[300005];
vector<int> res;

void dijk(int nd, int k){
	priority_queue< plii, vector< plii >, greater< plii> > pq;
	ll d0,d1;
	int idx,nd1;
	pq.push({0,{nd,-1}});
	res.clear();
	res.resize(k);
	while(k--){
		d0 = pq.top().first;
		nd = pq.top().second.first;
		idx= pq.top().second.second;
		pq.pop();

		if(vis[nd]){
			k++;
			continue;
		}
		else
			res[k]=idx;

		for(int i=0;i<e[nd].size();i++){
			nd1= e[nd][i].v; 
			d1 = d0+e[nd][i].w;
			if(d[nd1] > d1){
				d[nd1]=d1;
				idx = e[nd][i].i;
				pq.push({d1,{nd1,idx}});
			}
		}
		vis[nd]=1;
	}
}

int main(){
	int n,m,k,i,x,y,w;
	cin >> n >> m >> k;
	k=min(k,n-1);
	cout<<k<<"\n";
	for(i=0;i<m;i++){
		scanf("%d %d %d",&x,&y,&w);
		e[x].push_back({y,w,i+1});
		e[y].push_back({x,w,i+1});
	}
	for(i=1;i<=n;i++){
		d[i]=inf;
	}
	d[1]=0;
	dijk(1,k+1);
	for(i=0;i<k;i++)
		printf("%d ",res[i]);
}