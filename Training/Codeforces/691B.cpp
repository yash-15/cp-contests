#include <bits/stdc++.h>
using namespace std;

int main(){
	
	set<char> sym{'A','H','I','M','O','T','U','V','W','X','Y','o','v','w','x'};
	map<char,char> mirr;
	mirr['b']='d';
	mirr['d']='b';
	mirr['p']='q';
	mirr['q']='p';
	string s;
	cin >> s;
	int i,j;
	for(i=0,j=s.length()-1;i<=j;i++,j--){
		if(s[i]==s[j]){
			if(sym.find(s[i])==sym.end())
				break;
		}
		else if(mirr[s[i]]!=s[j])
			break;
	}
	cout<<(i>j?"TAK":"NIE")<<"\n";
}
		
