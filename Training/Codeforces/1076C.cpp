#include <bits/stdc++.h>
using namespace std;
int main(){
	int t,d;
	cin >> t;
	while(t--){
		cin >> d;
		if(d<4 && d>0)
			puts("N");
		else
			printf("Y %.10f %.10f\n",
				(d+sqrt(d*d-4*d))/2,
				(d-sqrt(d*d-4*d))/2);
	}
}