#include <bits/stdc++.h>
using namespace std;

int x,y;
int a[1003][2];
int memo[1003][2];

int solve(int idx, int col){
	//cout<<idx<<","<<col<<" :: "<<"\n";
	if(idx<0)
		return 0;
	int temp = memo[idx][col];
	if(temp==-1)
		return 0;
	if(temp>0)
		return temp;
	int i,val = 0,ans = 1e8;
	for(i=0;i<x-1;i++)
		val += a[idx-i][col];
	for(;i<y && idx-i>=0;i++){
		val += a[idx-i][col];
		ans=min(ans,val+solve(idx-i-1,1-col));
	}
	memo[idx][col]=(ans?ans:-1);
	//cout<<"  "<<idx<<" "<<col<<" :: "<<ans<<"\n";
	return ans;
}

int main(){
	int n,m,i,j,k;
	cin >> n >> m >> x >> y;
	string s[n];
	for(i=0;i<n;i++)
		cin >> s[i];
	for(i=0;i<n;i++)
		for(j=0;j<m;j++)
			if(s[i][j]=='#')
				a[j][0]++;
			else
				a[j][1]++;
	cout << min(solve(m-1,0),solve(m-1,1));
}