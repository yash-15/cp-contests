#include <bits/stdc++.h>
using namespace std;
#define ll long long
int main(){
	int n;
	cin >> n;
	vector<pair<ll,int> > a(n+n);
	ll l,r;
	for(int i=0;i<n;i++){
		scanf("%lld %lld",&l,&r);
		a[i+i] = {l,1};
		a[i+i+1]={r+1,-1};
	}
	sort(a.begin(),a.end());
	ll x = 0;
	int cnt = 0;
	vector<ll> res(n+1);
	for(int i=0;i<n+n;i++){
		if(a[i].first<=x)
			cnt+=a[i].second;
		else{
			res[cnt]+=a[i].first-x;
			x=a[i].first;
			cnt+=a[i].second;
		}
	}
	for(int i=1;i<=n;i++)
		printf("%lld ",res[i]);
	puts("");
}
		
