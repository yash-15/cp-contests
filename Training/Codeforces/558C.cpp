#include <bits/stdc++.h>
using namespace std;

inline bool isP2(int x){
	return !(x&(x-1));
}

int a[100005],b[100005],pwr[100005];

int main(){
	int n;
	cin >> n;
	int ans=0;
	int i,j,k,mn=1e6;
	for(i=0;i<n;i++){
		scanf("%d",a+i);
		b[i]=a[i];
		while(!(b[i]&1)){
			b[i]>>=1;
			pwr[i]++;
		}
		mn=min(mn,b[i]);
	}
	bool change;
	do{
		change=0;
		for(i=0;i<n;i++){
			if(b[i]<mn){
				mn=b[i];
				change=1;
			}
			else if(b[i]%mn || !isP2(b[i]/mn)){
				ans+=pwr[i];
				pwr[i]=0;
				b[i]/=2;
				ans++;
				i--;
			}
		}
	}while(change);
	
	if(mn>1)
		for(i=0;i<n;i++)
			b[i]/=mn;
	
	map<int,int> logs;
	vector<int> f(20);
	
	for(i=0;i<20;i++)
		logs[1<<i]=i;
	for(i=0;i<n;i++)
		f[logs[b[i]]+pwr[i]]++;
	
	for(i=0,j=19;i<j;){
		k=min(f[i],f[j]);
		ans+=(j-i)*k;
		f[i]-=k;
		f[j]-=k;
		if(!f[i])
			i++;
		if(!f[j])
			j--;
	}		
	cout<<ans<<"\n";
}
