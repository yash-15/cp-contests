#include <bits/stdc++.h>
using namespace std;
#define ll long long
ll a[200005];
int dp[200005];
int main(){
	int n,m,k,i,j;
	cin >> n >> m >> k;
	for(i=0;i<n;i++)
		scanf("%lld",a+i+1);
	for(i=2;i<=n;i++)
		a[i]+=a[i-1];
	dp[n]=1;
	for(i=n-1;i>0;i--){
		j = lower_bound(a+i,a+n+1,a[i-1]+k+1)-a;
		dp[i]=1+dp[j];
		if(dp[i]>m)
			break;
	}
	printf("%d\n",n-i);
}