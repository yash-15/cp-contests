#include <bits/stdc++.h>
using namespace std;
int l[200005],r[200005];
int main(){
	int q,v,lc = 0, rc = 0;
	char ch;
	cin >> q;
	while(q--){
		scanf(" %c %d",&ch,&v);
		switch(ch){
			case 'L': l[v]=++lc;	break;
			case 'R': r[v]=++rc;	break;
			case '?': if(l[v])
						printf("%d\n",min(lc-l[v],rc+l[v]-1));
					  else
						printf("%d\n",min(rc-r[v],lc+r[v]-1));
		}
	}
}