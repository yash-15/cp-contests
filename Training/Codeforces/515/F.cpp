#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define INF 1e17

map<int, pair<int,int> > hor,ver;
map<int, pair<ll,ll> > dp_h,dp_v;
set<int> s;

ll solve(int l, int x, int y){
	ll ans = INF;
	if(hor.count(l))
		ans = min(dp_h[l].first + abs(x-hor[l].first) + abs(y-l),
					dp_h[l].second + abs(x-hor[l].second) + abs(y-l));
	if(ver.count(l))
		ans = min(ans,min(dp_v[l].first + abs(x-l) + abs(y-ver[l].first),
					dp_v[l].second + abs(x-l) + abs(y-ver[l].second)));
	return ans;
}

ll f(ll x){
	return (x==INF)?-1:x;
}

int main(){
	int n,i,j,x,y,mx;
	cin >> n;
	hor[0] = ver[0] = {0,0};
	dp_h[0] = dp_v[0] = {0,0};
	while(n--){
		scanf("%d %d",&x,&y);
		mx = max(x,y);
		s.insert(mx);
		if(y == mx){
			if(!hor.count(mx))
				hor[mx]={x,x};
			else{
				hor[mx].first = min(hor[mx].first,x);
				hor[mx].second = max(hor[mx].second,x);
			}
		}
		if(x == mx){
			if(!ver.count(mx))
				ver[mx]={y,y};
			else{
				ver[mx].first = min(ver[mx].first,y);
				ver[mx].second = max(ver[mx].second,y);
			}
		}
	}
	int prev = 0;
	for(auto l: s){
		if(!ver.count(l)){
			dp_h[l].first = solve(prev,hor[l].second,l)+(hor[l].second-hor[l].first);
			dp_h[l].second = solve(prev,hor[l].first,l)+(hor[l].second-hor[l].first);
			dp_v[l].first = dp_v[l].second = INF;
		}
		else if(!hor.count(l)){
			dp_v[l].first = solve(prev,l,ver[l].second)+(ver[l].second-ver[l].first);
			dp_v[l].second = solve(prev,l,ver[l].first)+(ver[l].second-ver[l].first);
			dp_h[l].first = dp_h[l].second = INF;
		}
		else{
			dp_h[l].first = solve(prev,l,ver[l].first)+(l-hor[l].first)+(l-ver[l].first);
			dp_v[l].first = solve(prev,hor[l].first,l)+(l-hor[l].first)+(l-ver[l].first);
			dp_h[l].second = dp_v[l].second = INF;
		}
		prev = l;
		// cout<<f(dp_h[l].first)<<" "<<f(dp_h[l].second)<<" "
		// 	<<f(dp_v[l].first)<<" "<<f(dp_v[l].second)<<"\n";
	}
	cout << min(min(dp_h[prev].first,dp_h[prev].second),
				min(dp_v[prev].first,dp_v[prev].second))<<"\n";
	
}