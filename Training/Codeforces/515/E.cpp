#include <bits/stdc++.h>
using namespace std;
#define mod 998244353
#define ll long long

char a[200005],b[200005];
int main(){
	int n,m,i,j,k;
	scanf("%d %d %s %s",&n,&m,a,b);
	reverse(a,a+n);	
	reverse(b,b+m);
	for(;n<m;n++)
		a[n]='0';
	for(;m<n;m++)
		b[m]='0';
	a[n]=b[n]=0;
	ll ans = 0, cur = 0, pwr = 1;
	for(i=0;i<n;i++){
		cur = (cur+pwr*(a[i]-'0'))%mod;
		pwr = (pwr*2)%mod;
		if(b[i]=='1')
			ans += cur;
	}
	ans%=mod;
	cout<<ans<<"\n";
}