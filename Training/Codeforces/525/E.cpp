#include <bits/stdc++.h>
using namespace std;

#define ll long long
#define N 300005

int a[N];
vector<int> e[N];
ll dp[N];

ll _mx=-1,_cnt=0;

void postOrder(int nd, int par, bool isUpdate){
	dp[nd]=0;
	for(auto z:e[nd])
		if(z!=par){
			postOrder(z,nd,isUpdate);
			dp[nd]+=max(0ll,dp[z]);
		}
	dp[nd] = max(0ll,a[nd]+dp[nd]);
	if(isUpdate)
		_mx = max(_mx,dp[nd]);
	else if(dp[nd]==_mx){
		_cnt++;
		dp[nd]=0;	
	}
}

int main(){
	int n,i,j,u,v;
	scanf("%d",&n);
	for(i=0;i<n;i++)
		scanf("%d",a+i+1);
	for(i=1;i<n;i++){
		scanf("%d %d",&u,&v);
		e[u].push_back(v);
		e[v].push_back(u);
	}
	int mx = a[1],cnt = 1;
	for(i=2;i<=n && mx<=0;i++){
		if(a[i]>mx)
			mx=a[i],cnt=1;
		else if(a[i]==mx)
			cnt++;
	}
	if(mx<=0)
		_mx=mx,_cnt=cnt;
	else{
		postOrder(1,0,1);
		postOrder(1,0,0);
	}
	printf("%lld %lld\n",_mx*_cnt,_cnt);
}