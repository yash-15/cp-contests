#include <bits/stdc++.h>
using namespace std;

int X = 0, Y = 0;
int num = 0;
int query(int x, int y){
	num++;
	//printf("? %d %d\n",x,y);
	//fflush(stdout);
	int res=0;
	if((X^x) > (Y^y))	res = 1;
	if((X^x) < (Y^y))	res = -1;
	//scanf(" %d",&res);
	return res;
}

void solve(int pos, int isGreater, int& lt, int& rt){
	//cout<<pos<<" "<<isGreater<<" "<<lt<<" "<<rt<<"\n";
	if(pos==0)
	{
		if(isGreater>0) // 1,0
			lt+=1,rt+=0;
		else if(isGreater<0) // 0,1
			lt+=0,rt+=1;
		else{
			int cmp = query(lt+1,rt);
			if(cmp > 0) // 1,0 <- 0,0
				return;
			else if(cmp < 0) // 0,1 <- 1,1
				lt+=1,rt+=1;
		}
		return;
	}
	if(isGreater>0){
		int aux = query(lt+(1<<pos)-1,rt+(1<<pos)-1);
		if(aux>0){  // 1,0 case
			lt+= (1<<pos);
			solve(pos-1,query(lt,rt),lt,rt);
			return;
		}
	}
	if(isGreater>=0){ // 0,0 or 1,1
		int msb = query(lt+(1<<pos),rt);
		if(msb>0) // 1,0 <- 0,0
			solve(pos-1,isGreater,lt,rt);
		else{ // 0,1 <- 1,1
			lt+=(1<<pos),rt+=(1<<pos);
			solve(pos-1,isGreater,lt,rt);
		}
	}
	else if(isGreater<0){
		swap(X,Y);
		solve(pos,1,rt,lt);
	}
}


int main(){
	cin >> X >> Y;
	int isGreater = query(0,0), a=0, b=0;
	solve(30-1,isGreater,a,b);
	printf("! %d %d\n",a,b);
	cout<<num<<"\n";
	fflush(stdout);
}