#include <bits/stdc++.h>
using namespace std;

bool foundByEthan(string a, string b){
	int l1 = a.length(),l2=b.length();
	int i=0,j=0;
	while(1){
		if(i>=l1)
			return 1;
		if(j>=l2)
			return 0;
		if(a[i]==b[j])
			i++,j++;
		else if(i==0)
			j++;
		else
			i=0;
	}
}

int main(){
	int tt;
	cin >> tt;
	for(int t=1;t<=tt;t++){
		string a;
		cin >> a;
		cout<<"Case #"<<t<<": ";
		int n = a.length();
		int k;
		for(k=1;k<n;k++)
			if(a[k]==a[0])
				break;
		string b = a.substr(0,k)+a;
		cout<<((foundByEthan(a,b))?"Impossible":b)<<"\n";

	}
}