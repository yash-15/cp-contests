#include <bits/stdc++.h>
using namespace std;

priority_queue< pair<int,int> > pq; //freq,rank
		
void getResult(int k, int res[]){
	int freq,rank;
	while(k--){
		freq = -pq.top().first;
		rank = -pq.top().second;
		pq.pop();
		res[k] = rank;
		pq.push(make_pair(-(freq+1),-rank));
	}
}

int main()
{
	int tt;
	cin >> tt;
	for(int t=1;t<=tt;t++){
		int n,k;
		long long v;
		cin >> n >> k >> v;
		v = 1+ (v-1)%(n*k);
		int res[50];
		string a[50];
		while(!pq.empty())
			pq.pop();
		for(int i=0;i<n;i++){
			cin >> a[i];
			pq.push(make_pair(0,-i));
		}
		while(v--)
			getResult(k,res);
		sort(res,res+k);
		cout<<"Case #"<<t<<":";
		for(int i=0;i<k;i++)
			cout<<" "<<a[res[i]];
		cout<<"\n";
	}
}