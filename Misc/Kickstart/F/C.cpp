#include <bits/stdc++.h>
using namespace std;

#define ll long long

std::string str;

bool isPal(int n){
	int m = str.length();
	if(m>n)
		return 0;
	for(int i = n-m;i<n-1-i;i++)
		if(str[i]!=str[n-1-i])
			return 0;
	return 1;
}

ll pwr(ll a, ll p){
	if(p==2)
		return a*a;
	if(p==1)
		return a;
	if(p==0)
		return 1;
	ll x = pwr(a,p/2);
	if(p&1)
		return x*x*a;
	else
		return x*x;
}

// if a^p > k return -1
// else a^p

ll pwrLessK(ll a, ll p, ll k) {
	ll k2=k,p2=p;
	while(p2-- && k2)
		k2/=a;
	if(k2==0)
		return -1;
	else
		return pwr(a,p);
}

// return -1 if f()>k
// return f() otherwise
ll f(int n,int l, ll k, int p){
	int dof;
	if(n%2)
		dof = n/2+1-p;
	else
		dof = n/2 - p;
	//cout<<"\t\tdof: "<<dof<<"\n";
	if(dof>=0)
		return pwrLessK(l,dof,k);
	else{
		if(!isPal(n))
			return 0;
		else if (k==0)
			return -1;
		else
			return 1;
	}
}


// return length of kth palindrome
// return 1 if skip=true and kth pal exists
int solve(int n, int l, ll k, bool skip=false){
	ll total = 0, curr, k2;
	bool found;
	int ch;
	str = "";
	for(int pos=1;pos<=n;pos++){
		str+="a";
		for(ch = 0;ch < l;ch++){
			found =false;
			str[pos-1]='a'+ch;
			if(isPal(pos))
				k--;
			if(!k)
				return pos;
			k2=k;
		//	cout<<"Finding "<<k<<"th ele for prefix:\t "<<str<<"\n";
			for(int len=pos+1;len<=n;len++){
				curr = f(len,l,k2,pos);
		//		cout<<"   len "<<" "<<len<<" : "<<curr<<"\n";
				if(curr==-1){
					found = true;
					break;
				}
				else{
					k2-=curr;
				}
			}
			if(found || k2 == 0)
				break;
			else{
				k=k2;
			}
		}
		if(skip)
			return 1;
		if(ch==l)
			return 0;
	}
	return 0;
}

int superSolve(ll n, int l, ll k){
	if(!solve(n,l,k,true))
		return 0;
	ll low = 0, hi = n,mid;
	while(hi-low>1){
		mid = (low+hi)/2;
		cout<<low<<" "<<mid<<" "<<hi<<"\n";
		if(k<=n-mid)
			hi = mid;
		else if(solve(mid,l,k-(n-mid),true)){
			cout<<"Solution exists!";
			hi = mid;
		}
		else
			low = mid;
	}
	cout<<"After!\n";
	cout<<low<<" "<<mid<<" "<<hi<<"\n";

	cout<<" hi:: "<<hi<<"\n";
	return (n-hi)+solve(hi,l,k-(n-hi));

}

int main() {
	int t,tt;
	cin >> t;
	for(tt=1;tt<=t;tt++){
		int l;
		int ans = 0;
		ll n,k;
		cin >> l >> n >> k;
		if(l==1){
			if(n>k)
				ans = 0;
			else
				ans = n;
		}
		else
			ans = superSolve(n,l,k);	    
	    cout<<"Case #"<<tt<<": "<<ans<<"\n";
		
	}
	return 0;
}
