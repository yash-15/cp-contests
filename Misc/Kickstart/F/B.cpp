#include <bits/stdc++.h>
using namespace std;

#define ll long long

ll d[51][51];

ll solve(int mask, int n){
	vector<int> s1,s2;
	for(int i=0;i<n;i++)
		if(mask & (1<<i))
			s1.push_back(i);
		else
			s2.push_back(i);
	ll ans = 0,cur;
	for(auto x:s1){
		cur= 5e8;
		for(auto y:s2)
			cur = min(cur,d[x][y]);
		ans+=cur;
	}
	for(auto x:s2){
		cur= 5e8;
		for(auto y:s1)
			cur = min(cur,d[x][y]);
		ans+=cur;
	}
	if(ans ==15){
		for (auto x:s1)
			cout<<x+1<<",";
		cout<<"\n";
	}
	return ans;
}

int main() {
	int t,tt;
	cin >> t;
	for(tt=1;tt<=t;tt++){
		int n,e,i,j,k,u,v,w;
		cin >> n >> e;
		for(i=0;i<n;i++){
			for(j=0;j<n;j++)
				d[i][j]=1e8;
			d[i][i]=0;
		}
		while(e--){
			scanf("%d%d%d",&u,&v,&w);
			d[u-1][v-1]=d[v-1][u-1]=w;
		}
		for(k=0;k<n;k++)
			for(i=0;i<n;i++)
				for(j=0;j<n;j++)
					d[i][j]=min(d[i][j],d[i][k]+d[k][j]);

		ll curmin = -1;
		int cnt=0;
		for(int mask=0;mask<(1<<n);mask++){
			ll dist = solve(mask,n);
			if(curmin == -1 || dist<curmin){
				curmin = dist;
				cnt=1;
			}
			else if(dist == curmin){
				//cout << mask << " : " << dist<<"\n";
				cnt++;
			}
		}
		cout<<n<<"\n";
		for(i=0;i<n;i++)
				for(j=0;j<n;j++)
						if(d[i][j]>100)
							d[i][j]=-1;
		for(i=0;i<n;i++,puts(""))
			for(j=0;j<n;j++)
				cout<<d[i][j]<<" ";	

	    cout<<"Case #"<<tt<<": "<<cnt<<"\n";
		
	}
	return 0;
}
