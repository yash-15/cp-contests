#include <bits/stdc++.h>
using namespace std;

void solve(string a, set< pair<int,vector<int> > > &s){
	int n = a.length(),i,j,k;
	for(i=0;i<n;i++){
		vector<int> v(26,0);
		for(j=i;j<n;j++){
			v[a[j]-'A']++;
			s.insert(make_pair(i*n+j,v));
		}
	}
}

void solve2(string a, set< vector<int> > &s){
	int n = a.length(),i,j,k;
	for(i=0;i<n;i++){
		vector<int> v(26,0);
		for(j=i;j<n;j++){
			v[a[j]-'A']++;
			s.insert(v);
		}
	}
}

int main() {
	int t,tt,n;
	cin >> t;
	for(tt=1;tt<=t;tt++){
		string a,b;
		set< pair<int,vector<int> > > s1;
		set<vector<int> > s2;
	    cin >> n >> a >> b;
	    solve(a,s1);
	    solve2(b,s2);
	    int ans = 0;
	    for(auto& x: s1){
	    	if(s2.find(x.second)!=s2.end())
	    		ans++;
	    }

	    cout<<"Case #"<<tt<<": "<<ans<<"\n";
	}
	return 0;
}
