#include <bits/stdc++.h>
using namespace std;

int a[10000];

int main() {
	int t,tt,n,k;
	cin >> t;
	for(tt=1;tt<=t;tt++){
	    cin >> n >> k;
	    for(int i=0;i<n;i++)
	        scanf("%d",a+i);
	    sort(a,a+n);
	    int i=0,d=0,j,ans=0;
	    for(j=0;j<n;j++){
	        
	        if(a[j]>d){
	            ans++;
	            i++;
	        }
	        if(i==k){
	            d++;
	            i=0;
	        }
	    }
	    cout<<"Case #"<<tt<<": "<<ans<<"\n";
	}
	return 0;
}
