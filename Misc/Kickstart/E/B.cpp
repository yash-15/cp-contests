#include <bits/stdc++.h>
using namespace std;

unordered_set<bitset<100>> na;
int cnt[2][101];
vector<int> edg;
map<int,bitset<100> > mp;
bitset<100> zero;
struct node{
    int wt;
    bitset<100> nd;

    node():wt(0),nd(0){}
    node(int _wt,bitset<100> _nd):wt(_wt),nd(_nd){}
};
class compareClass{
public:
    bool operator()(node nd1, node nd2){
        return nd1.wt<nd2.wt;
    }
};
priority_queue<node,vector<node>, compareClass> res;

int bfs(int p){
    while(!res.empty()){
        int wt = -res.top().wt;
        bitset<100> nd  = res.top().nd;
        res.pop();
        //cout << wt <<" ";
        //for(int i=0;i<p;i++)
        //    cout << nd[i];cout<<"\n";
        if(na.find(nd^zero)==na.end())
            return wt;

        for(int i=0;i<p;i++){
            if(nd[i]==0){
                nd[i]=1;
                res.push(node(-(wt+edg[i]),nd));
                nd[i]=0;
            }
        }
    }
}

int main()
{
	int t,tt,n,m,p,i,j,k;
	cin >> t;
    for(tt=1;tt<=t;tt++){
    	cin >> n >> m >> p;
    	memset(cnt,0,sizeof cnt);
    	bitset<100> a[n],b;
    	na.clear();
        edg.clear();
        mp.clear();
        while(!res.empty()) res.pop();
        zero = 0;
    	for(i=0;i<n;i++){
    	    cin >> a[i];
    	    for(j=0;j<p;j++)
                cnt[1-a[i][j]][j]++;
    	}
//    	for(i=0;i<p;i++)
//            cout<<cnt[0][i]<<" ";cout<<"\n";
//    	for(i=0;i<p;i++)
//            cout<<cnt[1][i]<<" ";cout<<"\n";

        int ans = 0;
        for(i=0;i<p;i++){
            if(cnt[0][i]<=cnt[1][i]){
                ans+=cnt[0][i];
                cnt[1][i]-=cnt[0][i];
                cnt[0][i]=0;
                edg.push_back(cnt[1][i]);
            }
            else{
                ans+=cnt[1][i];
                cnt[0][i]-=cnt[1][i];
                cnt[1][i]=0;
                edg.push_back(cnt[0][i]);
                zero[i]=1;
            }
        }
//        for(i=0;i<p;i++)
//            cout<<cnt[0][i]<<" ";cout<<"\n";
//    	for(i=0;i<p;i++)
//            cout<<cnt[1][i]<<" ";cout<<"\n";

    	for(i=0;i<m;i++){
    		cin >> b;
    		na.insert(b);
    	}
    	res.push(node());
    	ans+=bfs(p);
    	cout<<"Case #"<<tt<<": "<<ans<<"\n";
    }
}
