#include <bits/stdc++.h>
using namespace std;
#define ll long long

int main() {
	int t,tt,n,p,i,j;
	string s;
	vector<string> vs;
	cin >> t;
	for(tt=1;tt<=t;tt++){
		vs.clear();
		cin >> n >> p;
		for(i=0;i<p;i++){
			cin >> s;
			for(j=0;j<vs.size();j++){
				int l1 = vs[j].length();
				int l2 = s.length();
				if(l1<=l2){
					if(s.substr(0,l1) == vs[j])
						break;
				}
				else{
					if(vs[j].substr(0,l2) == s){
						vs[j]=s;
						break;	
					}
				}
			}
			if(j==vs.size())
				vs.push_back(s);
		}

		ll ans = 1,one = 1;
		ans = (ans<<n);
		for(i=0;i<vs.size();i++)
			ans -= (one<<(n-vs[i].length()));
	    cout<<"Case #"<<tt<<": "<<ans<<"\n";
	}
	return 0;
}
