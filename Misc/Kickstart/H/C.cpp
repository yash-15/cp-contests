#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define mod 1000000007
ll fact[200005],inv[200005],p2[200005];

ll pwr(ll x, int p){
	ll ans = 1;
	while(p){
		if(p&1)
			ans = (ans*x)%mod;
		x=(x*x)%mod;
		p>>=1;
	}
	return ans;
}

int main() {
	int t,tt,n,m,i,j,k;
	cin >> t;
	for(tt=1;tt<=t;tt++){
		cin >> n >> m;
		fact[0]=1,inv[0]=1, p2[0]=1;
		for(i=1;i<=n+n;i++){
			fact[i]=(fact[i-1]*i)%mod;
			inv[i]=(pwr(fact[i],mod-2))%mod;
			p2[i]=(2*p2[i-1])%mod;
		}
		ll ans = 0,mult = 1,curr;
		for(k=0;k<=m;k++){
			curr = (((fact[m]*inv[k])%mod)*((p2[k]*inv[m-k])%mod))%mod;
			curr = (curr*(fact[n+n-k]))%mod;
			//curr = (curr*(inv[k]))%mod;

			ans += curr*mult;
			while(ans<0)
				ans+=mod;
			while(ans>=mod)
				ans-=mod;
			mult = -mult;
		}
	    cout<<"Case #"<<tt<<": "<<ans<<"\n";
	}
	return 0;
}
