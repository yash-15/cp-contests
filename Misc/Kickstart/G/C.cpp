#include <bits/stdc++.h>
using namespace std;

#define ll long long

int g[102][102];
ll dp[102][102];

int n,m;
int dx[]={0,0,-1,1};
int dy[]={1,-1,0,0};


priority_queue< pair< int, pair<int,int> > > pq; 
int dij(int x, int y, int ex, int ey, int e){
	while(!pq.empty())
		pq.pop();
	int i,j,px,py;
	for(i=1;i<=n;i++)
		for(j=1;j<=m;j++)
			dp[i][j]=-1;
	dp[x][y]=0;
	pq.push({0,{x,y}});
	while(!pq.empty()){
		x = pq.top().second.first;
		y = pq.top().second.second;
		pq.pop();
		if(x==ex && y==ey)
			return e-dp[x][y];
		for(i=0;i<4;i++){
			px = x+dx[i];
			py = y+dy[i];
			if(px>=1 && px<=n && py>=1 && py<=m){
				if(g[px][py]>-100000){
					if(dp[px][py]>dp[x][y]-g[px][py] || dp[px][py]==-1){
						dp[px][py]=dp[x][y]-g[px][py];
						pq.push({-dp[px][py],{px,py}});
					}
				}
			}
		}
	}
	return -1;
}

int main() {
	int t,tt,i,j;
	ll ans;
	cin >> t;
	for(tt=1;tt<=t;tt++){
		int e,x,y,ex,ey;
		cin >> n >> m >> e >> x >> y >> ex >> ey;
	    for(i=1;i<=n;i++)
	    	for(j=1;j<=m;j++)
	    		scanf("%d",&g[i][j]);
	    ans = dij(x,y,ex,ey,e);
	    if(ans<0)
	    	ans=-1;
	    cout<<"Case #"<<tt<<": "<<ans<<"\n";

	}
	return 0;
}
