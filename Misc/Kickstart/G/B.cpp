#include <bits/stdc++.h>
using namespace std;
#define ll long long

int x[400005],y[400005],z[400005],l[400005],r[400005];
int q[100005];
int v[800005],f[800005];
ll a[4],b[4],c[4],m[4];
ll total;
int mn,mx;

vector< pair<int,int> > vec;
vector<ll> qs;
unordered_map<ll,int> res;

ll solve(ll k, int n){
	int low = mn, high = mx,mid;
	ll cnt;
	int i;
	while(low<high){
		mid = (low+high)/2;
		for(i=1,cnt=0;i<=n;i++)
			if(mid<l[i])
				continue;
			else if(mid>r[i])
				cnt+=r[i]-l[i]+1;
			else
				cnt+=mid-l[i]+1;
		// cout << low<<" "<<high<<" "<<mid<<" : "<<cnt << " "<<k<<"\n";
		if(cnt<k){
			low = mid+1;
		}
		else
			high = mid;
	}
	return high;
}

int main() {
	int t,tt,n,nq,i,j,k;
	cin >> t;
	for(tt=1;tt<=t;tt++){
		qs.clear();
		res.clear();
		cin >> n >> nq;
		cin >> x[1] >> x[2] >> a[1]>>b[1]>>c[1]>>m[1];
		cin >> y[1] >> y[2] >> a[2]>>b[2]>>c[2]>>m[2];
		cin >> z[1] >> z[2] >> a[3]>>b[3]>>c[3]>>m[3];
		for(i=3;i<=n;i++){
			x[i]=(a[1]*x[i-1]+b[1]*x[i-2]+c[1])%m[1];
			y[i]=(a[2]*y[i-1]+b[2]*y[i-2]+c[2])%m[2];
		}
		for(i=3;i<=nq;i++){
			z[i]=(a[3]*z[i-1]+b[3]*z[i-2]+c[3])%m[3];
		}
		total = 0;
		mn=1+min(x[1],y[1]);
		mx=1+max(x[1],y[1]);
		vec.resize(n+n);
		for(i=1;i<=n;i++){
			l[i]=1+min(x[i],y[i]);
			r[i]=1+max(x[i],y[i]);
			total += r[i]-l[i]+1;
			mn=min(mn,l[i]);
			mx=max(mx,r[i]);
			vec[2*i-2] = {l[i],1};
			vec[2*i-1] = {r[i]+1,-1};
			// cout<<l[i]<<" -- "<<r[i]<<"\n";
		}
		sort(vec.begin(), vec.end());
		v[0]=mn;
		f[0]=0;
		for(i=0,k=0;i<n+n;i++){
			if(vec[i].first == v[k]){
				f[k]+=vec[i].second;
			}
			else{
				v[++k]=vec[i].first;
				f[k]=f[k-1]+vec[i].second;
			}
		}

		for(i=1;i<=nq;i++){
			q[i]=1+z[i];
			if(q[i]<=total)
				qs.push_back(total+1-q[i]);
		}
		sort(qs.begin(), qs.end());
		ll cur = 0;
		i=0;
		for(auto _q: qs){
			while(cur+(v[i+1]-v[i])*1ll*f[i] < _q){
				cur+=(v[i+1]-v[i])*1ll*f[i];
				i++;
			}
			res[_q]=v[i]+(_q-cur-1)/f[i];
		}

		ll ans = 0;
		for(i=1;i<=nq;i++){
			q[i]=1+z[i];
			if(q[i]<=total)
				ans += i*1ll*res[total+1-q[i]];
		}
	    cout<<"Case #"<<tt<<": "<<ans<<"\n";
	}
	return 0;
}
