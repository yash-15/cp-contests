#include <bits/stdc++.h>
using namespace std;
#define ll long long

int a[7003];
map<int,int> f;

ll brute(int n){
	int ans=0;
	for(int i=0;i<n;i++)
		for(int j=i+1;j<n;j++)
			for(int k=j+1;k<n;k++)
				if(a[k]==a[i]*a[j] || a[i]==a[j]*a[k] || a[j]==a[i]*a[k])
					ans++;
	return ans;
}
int main() {
	int t,tt,n,i,j;
	cin >> t;
	for(tt=1;tt<=t;tt++){
		f.clear();
		cin >> n;
		for(i=0;i<n;i++){
			scanf("%d",a+i);
			f[a[i]]++;
		}
		sort(a,a+n);
		int c0,c1;
		ll ans = 0;
		for(i=0;i<n;i++)
			if(a[i]>0)
				break;
		c0=i;
		for(;i<n;i++)
			if(a[i]>1)
				break;
		c1=i-c0;
		for(;i<n;i++)
			for(j=i+1;j<n;j++)
				ans+=f[a[i]*a[j]];
		//	cout<<c0<<" "<<c1<<" :: "<<ans<<"\n";
		ans += (c0*(c0-1ll)*(c0-2ll))/6; // all 3 zeroes
		//cout<<"  "<<ans<<"\n";
		ans += (c1*(c1-1ll)*(c1-2ll))/6; // all 3 ones
		//cout<<"  "<<ans<<"\n";
		ans += ((c0*(c0-1ll))/2)*(n-c0); // two zeros and one other
		// one one and two other equals (>1)
		for(auto &x:f)
			if(x.first>1)
				ans+=c1*(x.second*(x.second-1ll))/2;
		//cout<<"  "<<ans<<"\n";
		
		ans = brute(n);
	    cout<<"Case #"<<tt<<": "<<ans<<"\n";
	}
	return 0;
}
