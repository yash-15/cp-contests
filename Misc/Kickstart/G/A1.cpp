#include <bits/stdc++.h>
using namespace std;
#define ll long long

int a[7003];

int brute(int n){
	int ans1=0,ans2=0,ans3=0;
	for(int i=0;i<n;i++)
		for(int j=i+1;j<n;j++)
			for(int k=j+1;k<n;k++){
				if(a[k]==a[i]*a[j])
					ans1++;
				if(a[i]==a[j]*a[k])
					ans2++;
				if(a[j]==a[i]*a[k])
					ans3++;
			}
	return max(ans1,max(ans2,ans3));
}
int main() {
	int t,tt,n,i,j;
	cin >> t;
	for(tt=1;tt<=t;tt++){
		cin >> n;
		for(i=0;i<n;i++)
			scanf("%d",a+i);
		int ans = brute(n);
	    cout<<"Case #"<<tt<<": "<<ans<<"\n";
	}
	return 0;
}
